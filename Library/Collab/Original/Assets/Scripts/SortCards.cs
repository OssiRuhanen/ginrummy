﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class SortCards : MonoBehaviour
{
    int sortSpot = 0;
    int[] cardsInHand = new int[20];
    // Start is called before the first frame update

    public void SortCardsButton()
    {
        sortSpot = 0;
        GameObject hand;
        // Find deadwood script
        GameObject sceneController = GameObject.Find("SceneController");
        Deadwood deadwood = sceneController.GetComponent<Deadwood>();
        //First count deadwood so we know the hits we want to sort
        deadwood.CountDeadwood();
        if (DropZone.turn == 0)
        {
            hand = GameObject.Find("Hand");
        }
        else
        {
            hand = GameObject.Find("EnemyHand");
        }
        System.Array.Reverse(deadwood.countedCompletedSetsEndgame);
        //Find hits from countedCompletedSetsEndgame and move cards
        MoveCards((deadwood.countedCompletedSetsEndgame), hand);    
        
        // Sort rest of the cards that didn't hit sets
        // First move all of those cards to an array as numbers
        for (int m = 0 + sortSpot; m < hand.transform.childCount; m++)
        {
            Sprite card = hand.transform.GetChild(m).GetChild(0).GetComponent<Image>().sprite;  // get card sprite
            int x = 0;
            while (x < 52)
            {
                if (card.name == "Deck_" + x)   // Try all sprite names to find out what card it is
                {
                    cardsInHand[m] = x + 1;         // Set cardsInHand array to the hand found
                                                    // +1 cause sprites start from deck_0
                }
                x++;
            }
        }
        
        // Move cards to numeric order
        int[] sortedCards = new int[20];
        int i1 = 0;
        for (int i = 13; i >= 1; i--)
        {
            for (int pos = 0; pos < cardsInHand.Length; pos++)
            {  
                if (deadwood.Face(cardsInHand[pos]) == i && cardsInHand[pos] != 0 && deadwood.countedCompletedSetsEndgame.Contains(cardsInHand[pos]) == false)
                {
                    sortedCards[i1] = cardsInHand[pos];
                    i1++;
                }
            }
        }
        deadwood.printCards(sortedCards);
        Debug.Log("Moving other cards");
        MoveCards(sortedCards, hand);


        // SORT BY COLOR
        //sortedCards[pos] = cardsInHand.Max(); // find max value and move it to sortedCards
        //int p = Array.IndexOf(cardsInHand, sortedCards[pos]); // find position of that value
        //Debug.Log(cardsInHand[p]);
        //cardsInHand[p] = 0; // delete the value
        //pos++;           

    }
    // Checks array for cards and finds that card from hand by checking sprite names
    // Sorts cards in order with sortSpot
    public void MoveCards(int[] array, GameObject hand )
    {
        foreach (int card in array)
        {
            if (card > 0)   // only do this on hits
            {
                //Debug.Log("Kortti: "+card);
                int spriteName = card - 1;  // Sprites start at 0
                string name = "Deck_" + spriteName;
                for (int i = 0; i < hand.transform.childCount; i++)
                {
                    // Compare sprites name to card in sets
                    if (name == hand.transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite.name)
                    {
                        Debug.Log(name);
                        Debug.Log(sortSpot);
                        // Set index so that the card goes to right place
                        hand.transform.GetChild(i).SetSiblingIndex(sortSpot);
                        sortSpot++;
                    }
                }
            }
        }
    }
}
