﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    bool stopdrag = false;
    public static bool showButton = false;
    public Transform parentToReturnTo = null;
	public Transform placeholderParent = null;
    public static bool notFromHand = false;
	GameObject placeholder = null;


    public void OnBeginDrag(PointerEventData eventData) {   // When you begin to drag a card

        if (gameObject.transform.parent.tag == "Draw" && DropZone.playCard == false)       // if you draw a card it sets parent to hand
        {
            notFromHand = true;
            DropZone.drawCard = true;
            Debug.Log("Setting parent");
            if (DropZone.turn == 0)
            {
                this.transform.SetParent(GameObject.Find("Hand").transform);    // Set parent to whos turn it is
            }
            else
            {
                this.transform.SetParent(GameObject.Find("EnemyHand").transform);  
            }
            
        }
        

            Debug.Log ("OnBeginDrag");    
            placeholder = new GameObject();
            placeholder.transform.SetParent(this.transform.parent);
            LayoutElement le = placeholder.AddComponent<LayoutElement>();
            le.preferredWidth = this.GetComponent<LayoutElement>().preferredWidth;
            le.preferredHeight = this.GetComponent<LayoutElement>().preferredHeight;
            le.flexibleWidth = 0;
            le.flexibleHeight = 0;
            placeholder.transform.SetSiblingIndex(this.transform.GetSiblingIndex());
            parentToReturnTo = this.transform.parent;
            placeholderParent = parentToReturnTo;
            this.transform.SetParent(this.transform.parent.parent);
            GetComponent<CanvasGroup>().blocksRaycasts = false;

	}
	
	public void OnDrag(PointerEventData eventData) {

        this.transform.position = eventData.position;
		if(placeholder.transform.parent != placeholderParent)
			placeholder.transform.SetParent(placeholderParent);
		int newSiblingIndex = placeholderParent.childCount;

		for(int i=0; i < placeholderParent.childCount; i++) {
			if(this.transform.position.x < placeholderParent.GetChild(i).position.x) {

				newSiblingIndex = i;

				if(placeholder.transform.GetSiblingIndex() < newSiblingIndex)
					newSiblingIndex--;

				break;
			}
		}

		placeholder.transform.SetSiblingIndex(newSiblingIndex);

	}
	
	public void OnEndDrag(PointerEventData eventData) {

        if (placeholder.transform.parent.name == "Tabletop" && DropZone.drawCard == true)        // Move card to top of tabletop
        {
            Debug.Log("Tabletop");
            this.transform.SetParent(parentToReturnTo);
            this.transform.SetAsLastSibling();
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            DropZone.playCard = true;

        }   
        else    // set parent so the card goes to right place
        {
            this.transform.SetParent(parentToReturnTo);
            this.transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());
            GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
        Destroy(placeholder);
	}
	
	
	
}
