﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Deadwood : MonoBehaviour
{
    public GameObject HandCards;
    int notDeadwoodCount = 0;
    bool cantFindMore = false;
    bool startingcardPlaced = false;
    int[] CardsInHand = new int[11];
    int[] deleteThese = new int[11];    // vois alustaa arvot vaikka 999 ettei tuu ongelmia 0 kans
    int[] deleteThesePlaceholder = new int[11];
    int[] countedCompletedSets = new int[11];
    int[] compareCards = new int[11];
    // Start is called before the first frame update
    void Start()
    {
        CardsToArray();
        notDeadwoodCount = 0;

        for (int k = 0; k < CardsInHand.Length; k++) // clear placeholder
        {
            CardsInHand[k] = 999;
        }
        for (int k = 0; k < deleteThese.Length; k++) // clear placeholder
        {
            deleteThese[k] = 999;
        }
        for (int k = 0; k < deleteThese.Length; k++) // clear placeholder
        {
            deleteThesePlaceholder[k] = 999;
        }
        for (int k = 0; k < deleteThese.Length; k++) // clear placeholder
        {
            countedCompletedSets[k] = 999;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Returns card suit int 0,1,2 or 3
    int Suit(int card)
    {
        int suit = card / 13;
        return suit;
    }
    // Returns card face 0-12
    int Face(int card)  
    {
        int face = card % 13 +1;
        return face;
    }
    // Checks all sprites in hand and puts them into CardsInHand
    void CardsToArray()
    {
        int i = 0;
        while (i < HandCards.transform.childCount)
        {
            Sprite Card = HandCards.transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite;  // get card sprite
            int x = 0;
            while (x < 52)
            {
                if (Card.name == "Deck_" + x)   // Try all sprite names to find out what card it is
                {
                    CardsInHand[i] = x;         // Set CardsInHand array to the hand found
                }
                x++;
            }
            i++;
        }
    }

    // Finds higher face card
    int FindHigherStraight(int Card)   
    {
        for (int i = 0; i < CardsInHand.Length; i++)
        {
            if (Card == CardsInHand[i] - 1 && Suit(Card) == Suit(CardsInHand[i]))   // Test if card is one higher and same suit
            {
                return CardsInHand[i];
            }
        }
        cantFindMore = true;
        return 0;
    }


    int FindPair(int Card)  // Search for same numbered cards
    {
        int x = 0;
        for (int i = 0; i < CardsInHand.Length; i++)    // Check all cards
        {
            if (Face(Card) == Face(CardsInHand[i]) )    // If card has the same number as card in hand, first one always hits
            {
                deleteThesePlaceholder[x] = CardsInHand[i]; // Put the card in deleteThesePlaceholder
                x++;
            }
        }
        // if cards are found, placing them in deleteThese
        if (CountArray(deleteThesePlaceholder) > 2)  // Check if array has 3 or more cards
        {
            int z = 0;
            for (int b = 0; b < deleteThesePlaceholder.Length; b++)
            {
                if (deleteThesePlaceholder[b] < 999)
                {
                    deleteThese[z] = deleteThesePlaceholder[b];
                    z++;
                }
            }
            MoveToCountedDeadwood(deleteThese);
        }
        for (int k = 0; k < deleteThesePlaceholder.Length; k++) // clear placeholder
        {
            deleteThesePlaceholder[k] = 999;
        }
        return 0;
    }

    public void CountDeadwood() // Count deadwood from hand
    {
        int deadwood1 = 0;
        int deadwood2 = 0;

        CardsToArray();
        CountPairedDeadwood();
        CountStraightDeadwood();
        deadwood1 = deadwoodCompare();
        Start();
        CountStraightDeadwood();
        CountPairedDeadwood();
        deadwood2 = deadwoodCompare();

        printCards(countedCompletedSets);


        if (deadwood1<deadwood2)
        {
            Debug.Log("Deadwood: " + (deadwood1 - 1));
        }
        else
        {
            Debug.Log("Deadwood: " + (deadwood2 - 1));
        }



    }

        public void CountStraightDeadwood()     // TÄÄ PITÄÄ SIIVOTA PAREMMAKS, IHAN PASKAA!
    {
        CardsToArray();
        bool startingcardPlaced = false;
        int x = 0;
        // start checking cards from first to last
        for (int i = 0; i < CardsInHand.Length; i++)    
        { 
            while (cantFindMore == false)   // search for higher card
            {
                int HigherCard = FindHigherStraight(CardsInHand[i]);    
                while (cantFindMore == false)
                {
                    if (startingcardPlaced == false)
                    {
                        deleteThesePlaceholder[x] = HigherCard - 1;  // starting card to placeholder
                        startingcardPlaced = true;
                    }               
                    if (x<10)
                    {
                        deleteThesePlaceholder[x + 1] = HigherCard;    // Higher card to destroy array
                    }
                    else if (x == 10)
                    {
                        deleteThesePlaceholder[x] = HigherCard;    // Higher card to destroy array
                    }
                    HigherCard = FindHigherStraight(HigherCard);
                    if (x < 10)
                    {
                        x++;
                    }
                }
                
            }

            // Cards are found, setting variables for next cards...
            if (CountArray(deleteThesePlaceholder) >2)  // Check if array has 3 or more cards
            {
                int z = 0;
                for (int b = 0; b < deleteThesePlaceholder.Length; b++)
                {
                    if (deleteThesePlaceholder[b] < 999)
                    {

                        deleteThese[z] = deleteThesePlaceholder[b];
                        z++;
                    }
                }
                MoveToCountedDeadwood(deleteThese);
            }
            
            for (int k = 0; k < deleteThesePlaceholder.Length; k++) // clear placeholder
            {
                deleteThesePlaceholder[k] = 999;
            }
            startingcardPlaced = false;
            cantFindMore = false; 
        } 
    }

    public void CountPairedDeadwood()   // Search for cards with same number
    {
        for (int i = 0; i < CardsInHand.Length; i++)
        {
            FindPair(CardsInHand[i]);
        }
    }

    // Count the amount of under 999 cards in array
    int CountArray(int[] array) 
    {
        int count = 0;
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i]<999)
            {
                count++;
                //Debug.Log("CountArray= " + count);
            }
        }
        return count;
    }

    // Print cards
    public void printCards(int[] cards) 
    {

        foreach (var item in cards)
        {
            printCard(item);
        }
    }
    public void printCard(int card)
    {
        if (Suit(card) == 0)
        {
            Debug.Log(Face(card) + " Spades");
        }
        if (Suit(card) == 1)
        {
            Debug.Log(Face(card) + " Hearts");
        }
        if (Suit(card) == 2)
        {
            Debug.Log(Face(card) + " Diamonds");
        }
        if (Suit(card) == 3)
        {
            Debug.Log(Face(card) + " Clubs");
        }
        
    }

    // Moves 3 or more cards to completed sets
    public void MoveToCountedDeadwood(int[] cards)  
    {
        for (int i = 0; i < cards.Length; i++)
        {
            if (cards[i] < 999)
            {
                if (CheckIfItsInArray(countedCompletedSets, cards[i]) == false) // Checking if array already contains card
                {
                    countedCompletedSets[notDeadwoodCount] = cards[i];
                    notDeadwoodCount++;
                }
               
            }
        }
    }
    // Checking if array already contains card
    bool CheckIfItsInArray(int[] cards, int card) 
    {
        bool found = false;
        for (int i = 0; i < cards.Length; i++)
        {
            if (cards[i] == card)
            {
                found = true;
                //Debug.Log("Found From ARRAY");
            }
        }
        return found;
    }


    int deadwoodCompare()   // returns deadwood
    {
        int deadwood = 0;
        for (int i = 0; i < countedCompletedSets.Length; i++)   // Move cards to compareCards
        {
            compareCards[i] = countedCompletedSets[i];
        }
        deleteSets(compareCards);   // delete hits from comparecards

        for (int i = 0; i < compareCards.Length; i++)
        {
            if (compareCards[i] < 999)  // count all leftover cards
            {
                deadwood = deadwood + Face(compareCards[i]);
            }
        }
        return deadwood;
    }
    void deleteSets(int[] cards)   // Delete cards that are completed sets from array
    {
        for (int i = 0; i < CardsInHand.Length; i++)
        {
            if (CardsInHand.Contains(compareCards[i]) == true)
            {
                compareCards[i] = 999;
            }
            else
            {
                compareCards[i] = CardsInHand[i];   // this might be bugged trying to move deadwood to comparecards
            }
        }
    }
}

