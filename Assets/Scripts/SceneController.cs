﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public GameObject drawDeck; // Nää vois laittaa searchilla koska nyt joutuu määrittelemään dropzonessakin
    public GameObject enemyHand;
    public GameObject playerHand;
    public GameObject nextDeal;
    public GameObject endGame;
    public GameObject tabletop;
    public GameObject cardBacks;
    public Button playBots;
    public AIController s;
    public Text EnemyPoints;
    public Text PlayerPoints;
    public GameObject nextTurn;
    public GameObject winScreen;
    public const int cardAmount = 52;
    public static bool roundEnded;
    public static bool botEnded = false;
    [SerializeField] private Sprite[] images;

    int score = 0;
    int enderScore = 0;

    [Header("Point Settings")]
    public int penalty = 15;
    public int ginBonus = 20;
    public int ginBonus11 = 25;
    // Start is called before the first frame update
    void Start()
    {
        AIController.passedTurn = false;
        if (Grid.game.onlyBots)
        {
            if (Grid.game.debugMessages) Debug.Log("----------------------NEW ROUND-----------------------------------------------------------------");
            cardBacks.SetActive(false);
        }
        if (!Grid.game.loadScenes) ClearTable();
        Grid.game.Start();  // get all the variables
        SortCards sort = Grid.game.sort;
        UpdateScores();
        nextDeal.SetActive(false);  // Hide next deal button
        nextTurn.SetActive(false);  // Hide next turn button
        int c = 0;
        int[] numbers = new int[52];
        for (int x = 0; x < cardAmount; x++)  // make numbers array
        {
            numbers[x] = c;
            c++;
        }
        numbers = ShuffleArray(numbers);    // Shuffle numbers array for random numbers
        int i = 1;
        while (i < cardAmount+1)      // Shuffle card images
        {
            GameObject card = GameObject.Find("Card (" + i + ")");  // Find the Card(1) and so on...
            int index = i-1;
            int id = numbers[index];
            if (index<10) // set seenCards1 and 2
            {
                SeenCards.seenCards1[index] = id+1;
            }
            else if (index < 20 && index >9)
            {
                SeenCards.seenCards2[index] = id+1;
            }
            Transform art = card.transform.GetChild(0); // get the image child of the card
            art.GetComponent<Image>().sprite = images[id];  // get the image from images array and use randomized id
            i++;
        }
        drawDeck.transform.GetChild(0).SetParent(tabletop.transform);   // draw one card to tabletop
        if (tabletop.transform.childCount == 1)
        {
            nextTurn.SetActive(true);
            drawDeck.SetActive(false);
        }
        // sort the cards when game starts        
        sort.SortCardsButton();    
        if (DropZone.turn == 1)
        {
            s.PlayTurn();
            sort.SortCardsButton();
        }
        if (Grid.game.onlyBots && !Grid.game.runRoundOnce)
        {
            playBots.onClick.Invoke();
        }
    }
    private int[] ShuffleArray(int[] numbers)   // make random number array to draw images from image array
    {
        int[] newArray = numbers.Clone() as int[];
        for (int i = 0; i < newArray.Length; i++)
        {
            int tmp = newArray[i];
            int r = Random.Range(i, newArray.Length);
            newArray[i] = newArray[r];
            newArray[r] = tmp;
        }
        return newArray;
    }
    public void NextTurn()
    {
        AIController.passedTurn = true;
        DropZone.drawCard = false;  // set bools false for next round
        DropZone.playCard = false;
        DropZone.drawSecond = false;
        Draggable.notFromHand = false;
        if (drawDeck.transform.childCount == 0) // game is a draw
        {
            ResetScores();
            if (Grid.game.loadScenes)
            {
                SceneManager.LoadScene("Scene");
            }
            else
            {
                Start();
            }
        }
        if (DropZone.turn == 0) // set turn when pressing pass turn
        {
            DropZone.turn = 1;
            enemyHand.SetActive(true);
            if (Grid.game.tabletop.transform.childCount > 0)
            {
                // Save tabletop card to seencards
                SeenCards.CardtoSeenCards(Grid.game.tabletop.transform.GetChild
                    (Grid.game.tabletop.transform.childCount - 1).gameObject, 2);
            }
        }
        else
        {
            DropZone.turn = 0;
            // Hide enemy hand when turn changes   
            if (!Grid.game.onlyBots)
            {
                enemyHand.SetActive(false);
            }
            if (Grid.game.tabletop.transform.childCount>0)
            {
                // Save tabletop card to seencards
                SeenCards.CardtoSeenCards(Grid.game.tabletop.transform.GetChild
                    (Grid.game.tabletop.transform.childCount - 1).gameObject, 1);
            }
            
        }

        
        GameObject CardBack = GameObject.Find("CardBack");  // set CardBacks raycast false so you can draw
        CardBack.transform.GetComponent<Image>().raycastTarget = false;
        drawDeck.SetActive(true);
        nextTurn.SetActive(false);    // hide button
            
        if (DropZone.turn == 1)
        {
            // AI turn
            s.PlayTurn();
        }

    }
    public void RoundEnded()
    {
        //Grid.game.drawDeck.SetActive(true);
        //Grid.game.tabletop.SetActive(true);
        //Counting deadwood just in case player did something
        Grid.game.deadwood.CountDeadwood();
        enderScore = Grid.game.deadwood.publicDeadwood;
        roundEnded = true;
        enemyHand.SetActive(true);  // Show enemy hand when round ends
        if (Grid.game.onlyBots == false)
        {
            Grid.game.drawDeck.SetActive(false);
            Grid.game.tabletop.SetActive(false);         
        }
        if (!Grid.game.onlyBots)
        {
            cardBacks.SetActive(false);
        }     
        nextDeal.SetActive(true);
        endGame.SetActive(false);
        if (Grid.game.onlyBots == true && DropZone.turn == 1)
        {
            Grid.game.bot2.MoveDeadwoodToOpponentHand();
        }
        else if(DropZone.turn == 0)
        {
            Grid.game.bot1.MoveDeadwoodToOpponentHand();
        }
        else
        {
            botEnded = true;
        }
    }
    public void NextDeal() // Next deal button
    {
        SeenCards.slot1 = 0;
        SeenCards.slot2 = 0;
        Deadwood deadwood = Grid.game.deadwood;
        // Set turn to opposite to count opponents cards
        if (DropZone.turn == 0) // Player ended
        {
            // Counting opponents hands deadwood and submit it to score
            DropZone.turn = 1;
            deadwood.CountDeadwood();
            score = PlayerPrefs.GetInt("EnemyScore");
            if (deadwood.publicDeadwood <= enderScore)  // If opponent gets lower deadwood
            {
                score = PlayerPrefs.GetInt("PlayerScore");
                score = score + enderScore - deadwood.publicDeadwood + penalty;
                PlayerPrefs.SetInt("PlayerScore", score);   // Set playerscore cause penalty
            }
            else // Opponent has more deadwood
            {
                // if its a gin with 11 cards
                if (enderScore == 0 && deadwood.playerCards.transform.childCount >10)
                {
                    score = score + deadwood.publicDeadwood + ginBonus11;
                }
                // else it is a gin with 10 cards
                else if (enderScore == 0)
                {
                    score = score + deadwood.publicDeadwood + ginBonus;
                }
                else
                {
                    // Normal ending
                    if(Grid.game.debugMessages) Debug.Log("Normal ending");
                    score = score + deadwood.publicDeadwood - enderScore;
                }
                PlayerPrefs.SetInt("EnemyScore", score);
            }       
        }
        else
        {
            DropZone.turn = 0;  // Enemy ended
            deadwood.CountDeadwood();
            score = PlayerPrefs.GetInt("PlayerScore");
            if (deadwood.publicDeadwood <= enderScore)  // If opponent gets lower deadwood
            {
                score = PlayerPrefs.GetInt("EnemyScore");
                score = score + enderScore - deadwood.publicDeadwood + penalty;
                PlayerPrefs.SetInt("EnemyScore", score); // Set enemyscore cause penalty
            }
            else // Opponent has more deadwood
            {
                // if its a gin with 11 cards
                if (enderScore == 0 && deadwood.enemyCards.transform.childCount > 10)
                {
                    score = score + deadwood.publicDeadwood + ginBonus11;
                }
                // else it is a gin with 10 cards
                else if (enderScore == 0)
                {
                    score = score + deadwood.publicDeadwood + ginBonus;
                }
                else
                {
                    // Normal ending
                    if (Grid.game.debugMessages) Debug.Log("Normal ending");
                    score = score + deadwood.publicDeadwood - enderScore;
                }
                PlayerPrefs.SetInt("PlayerScore", score);
            }
           
        }
        roundEnded = false; // Setting roundEnded back to false for next round
        if (PlayerPrefs.GetInt("EnemyScore") >= 100 || PlayerPrefs.GetInt("PlayerScore") >= 100)
        {
            Grid.game.botRounds--;
            UpdateScores();
            if (PlayerPrefs.GetInt("EnemyScore") >= 100)
            {
                int add = PlayerPrefs.GetInt("LossAmount");
                add++;
                PlayerPrefs.SetInt("LossAmount", add);
            }
            else
            {
                int add = PlayerPrefs.GetInt("WinAmount");
                add++;
                PlayerPrefs.SetInt("WinAmount", add);  
            }
            ResetScores();
            winScreen.SetActive(true);
            if (Grid.game.onlyBots)
            {
                if (Grid.game.loadScenes) SceneManager.LoadScene("Scene");
                else
                {
                    Start();
                }
            }
        }
        else
        {
            if (Grid.game.loadScenes) SceneManager.LoadScene("Scene");
            else
            {
                Start();
            }
        } 
        Grid.game.waitLoad = true;
        

    }
    public void ResetScores()   // Reset PlayerPrefs when starting new game
    {
        PlayerPrefs.SetInt("PlayerScore", 0);
        PlayerPrefs.SetInt("EnemyScore", 0);
    }
    public void UpdateScores()
    {
        EnemyPoints.text = "Enemy points: " + PlayerPrefs.GetInt("EnemyScore").ToString();
        PlayerPoints.text = "Player points:  " + PlayerPrefs.GetInt("PlayerScore").ToString();
    }
    // Move cards back to starting positions
    public void ClearTable()
    {
        drawDeck.SetActive(true);
        tabletop.SetActive(true);
        cardBacks.SetActive(true);
        botEnded = false;
        for (int i = 1; i <= 52; i++)
        {
            if (i < 11)
            {
                GameObject.Find("Card (" + i + ")").transform.SetParent(playerHand.transform);
            }
            else if(i < 21)
            {
                GameObject.Find("Card (" + i + ")").transform.SetParent(enemyHand.transform);
            }
            else
            {
                GameObject.Find("Card (" + i + ")").transform.SetParent(drawDeck.transform);
            }          
        }
    }
    public void nextGame()
    {
        ResetScores();
        SceneManager.LoadScene("scene");
    }
    public void MainMenu()
    {
        ResetScores();
        SceneManager.LoadScene("Main Menu");
    }
}
