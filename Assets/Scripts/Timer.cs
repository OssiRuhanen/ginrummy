﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public static class Timer
{
    // Used this for timing, now using diagnostics
    public static Stopwatch stopWatch = new Stopwatch();
    static System.TimeSpan ts;


    public static void StopWatch()
    {
        stopWatch.Stop();
        ts = stopWatch.Elapsed;
        UnityEngine.Debug.Log(ts);
    }
    public static void StartWatch()
    {
        stopWatch.Start();
    }

}
