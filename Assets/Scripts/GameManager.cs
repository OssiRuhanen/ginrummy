﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    
    [HideInInspector]
    public SceneController sceneController;
    [HideInInspector]
    public Deadwood deadwood;
    [HideInInspector]
    public DropZone dropZone;
    [HideInInspector]
    public SortCards sort;
    [HideInInspector]
    public GameObject drawDeck;
    [HideInInspector]
    public GameObject tabletop;
    [HideInInspector]
    public GameObject cardBacks;
    [HideInInspector]
    public bool onlyBots;
    [HideInInspector]
    public bool waitLoad;


    public AIController bot1;
    public AIController2 bot2;
    public int botRounds;
    public bool runRoundOnce;
    public bool singleTurnsBot;
    public bool loadScenes;
    public bool debugMessages;

    public Text botRoundsText;
    
    
    public void Start()
    {
        sceneController = Object.FindObjectOfType<SceneController>();
        deadwood = sceneController.GetComponent<Deadwood>();
        dropZone = sceneController.GetComponent<DropZone>();
        sort = sceneController.GetComponent<SortCards>();
        drawDeck = GameObject.Find("Drawdeck");
        tabletop = GameObject.Find("Tabletop");
        cardBacks = GameObject.Find("CardBacks");
        bot1 = Object.FindObjectOfType<AIController>();
        bot2 = Object.FindObjectOfType<AIController2>();
        waitLoad = false;
        
    }
    public void LoadScenesButton()
    {
        loadScenes = false;
    }
    public void DebugMessagesButton()
    {
        loadScenes = true;
    }
    public void RunSingleTurnButton()
    {
        singleTurnsBot = true;
    }
    public void RunRoundOnceButton()
    {
        runRoundOnce = true;
    }
    public void SetBotRounds()
    {
        runRoundOnce = true;
    }
    public void SetBotPoints()
    {
        int.TryParse(botRoundsText.ToString(),out botRounds);
    }
}

