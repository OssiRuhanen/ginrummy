﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class AIController : MonoBehaviour
{
    public GameObject drawDeck;
    public GameObject hand;
    public GameObject tabletop;
    public GameObject playerHand;
    public GameObject endgame;
    public Text botAction;
    bool drawn = false;
    bool played = false;
    bool singleCard = false;
    public static bool passedTurn = false;

    public void DrawFromDeck()
    {
        if (drawn == false)
        {
            if (Grid.game.debugMessages) Debug.Log("Drawn " + hand.transform.GetChild(hand.transform.childCount-1).GetChild(0).GetComponent<Image>().sprite.name + "from drawdeck");
            botAction.text = "Bot draws from drawdeck";
            drawDeck.transform.GetChild(0).SetParent(hand.transform);
            drawn = true;
        }   
    }
    // Prints the drawn card to botAction text when bot keeps the tabletop card
    public void DrawFromTabletop()
    {
        if (drawn == false)
        {
            Deadwood deadwood = Grid.game.deadwood;
            // Printing card to botAction text
            Sprite cardSprite = hand.transform.GetChild(hand.transform.childCount - 1).GetChild(0).GetComponent<Image>().sprite;  // get card sprite
            int cardInt = 0;
            int x = 0;
            while (x < 52)
            {
                if (cardSprite.name == "Deck_" + x)   // Try all sprite names to find out what card it is
                {
                    cardInt = x + 1;         // Set cardsInHand array to the hand found
                    SeenCards.seenCards2[SeenCards.slot2] = cardInt;    // move to seen cards
                    SeenCards.slot2++;
                }
                x++;
            }
            int suit = deadwood.Suit(cardInt);
            string suitString = "";
            if (suit == 0)
            {
                suitString = "Spades";
            }
            if (suit == 1)
            {
                suitString = " Hearts";
            }
            if (suit == 2)
            {
                suitString = " Diamonds";
            }
            if (suit == 3)
            {
                suitString = " Clubs";
            }
            botAction.text = "Bot draws " + deadwood.Face(cardInt) +" " + suitString;
            drawn = true;
        }   
    }

    public void PlayTurn()
    {
        if (Grid.game.debugMessages) Debug.Log("-------------------------------------------------ENEMYBOT TURN");
        // Set booleans false
        drawn = false;
        played = false;
        singleCard = false;
        bool roundStart = false;
        // Find scripts
        Deadwood deadwood = Grid.game.deadwood;
        SortCards sort = Grid.game.sort;
        // Draw phase
        if (tabletop.transform.childCount == 1)
        {
            roundStart = true;  // Check if round just started
        }
        deadwood.CountDeadwood();
        int firstDeadwood = deadwood.publicDeadwood;
        if (tabletop.transform.childCount > 0)
        {
            // Move card to hand for now so deadwood calculations can be done
            tabletop.transform.GetChild(tabletop.transform.childCount - 1).SetParent(hand.transform);
        }
        // Count deadwood again for comparison without and with the tabletop card
        deadwood.CountDeadwood();
        int secondDeadwood = deadwood.publicDeadwood;
        if (firstDeadwood >= secondDeadwood)    // tabletop card hits
        {
            DrawFromTabletop();
            if (Grid.game.debugMessages) Debug.Log("Drawn " + hand.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite.name + "from tabletop");           
        }
        else    // If card didn't hit, put it back and draw from deck
        {
            if (roundStart == true && !passedTurn) // you can pass the first card in the deal
            {
                hand.transform.GetChild(hand.transform.childCount - 1).SetParent(tabletop.transform);
                passedTurn = true;
                Grid.game.sceneController.NextTurn();
                Debug.Log("SKIPPED");
                return;
            }
            hand.transform.GetChild(hand.transform.childCount-1).SetParent(tabletop.transform);
            DrawFromDeck();
            deadwood.CountDeadwood();
        }
        drawn = false;  // Set false so you can draw next turn
        // Bot always ends if its 10 or under, change this in next iterations
        if (deadwood.publicDeadwood == 0)
        {
            Grid.game.sceneController.RoundEnded();
        }
        else
        {
            // Find all singleCards
            int[] singleCards = FindNoHits();
            // if there is loneCard in hand
            if (singleCard == true)
            {
                // Find the highest loneCard and drop it
                // Could also compare all played cards so you drop something that has been played so its less outs
                // Do that to some other AI later
                if (Grid.game.debugMessages) Debug.Log("Singlecard drop");
                DropCard(FindHighestFace(singleCards), tabletop);
            }
            // If theres no singles to drop or they are too low drop something else
            if (played == false)
            {
                deadwood.CountDeadwood();   // Have to count again to get completed sets in order
                deadwood.DeleteSets(deadwood.cardsInHand);  // delete completed sets from cardsinHand
                if (Grid.game.debugMessages) Debug.Log("draw drop");
                DropCard(FindHighestFace(deadwood.cardsInHand), tabletop);
            }
            deadwood.CountDeadwood();
            // Bot always ends if its 10 or under, change this in next iterations
            if (deadwood.publicDeadwood <= 10)
            {
                sort.SortCardsButton();
                Grid.game.sceneController.RoundEnded();
                //GameObject.Find("EndGame").SetActive(false);
            }
            else
            {
                Grid.game.sceneController.NextTurn();
            }
        }
        
       
    }
    // Finds cards that dont make any draws
    public int[] FindNoHits()
    {
        Deadwood deadwood = Grid.game.deadwood;
        int[] returnCards = new int[20];
        deadwood.DeleteSets(deadwood.cardsInHand);  // delete sets from cardsinHand
        int[] cardsInHand = deadwood.cardsInHand;
        int z = 0;
        bool cardFound = false;
        int cards = hand.transform.childCount;
        // Go through all cards one at a time
        for (int i = 0; i <= cards; i++)
        {
            // Compare cardsInHand[i] to all cards in hand except the same card or 0
            for (int x = 0; x <= cards; x++)
            {
                // check so the card is not 0 or the same card you are checking or the compare card is not 0
                if (cardsInHand[i] != 0 && cardsInHand[i] != cardsInHand[x] && cardsInHand[x] != 0)
                {
                    // Check if there is same face cards in hand
                    if (deadwood.Face(cardsInHand[i]) == deadwood.Face(cardsInHand[x]))
                    {
                        // Should also check if there is a straight ? example TT9
                        cardFound = true;
                        x = +20;
                    }
                    // Check if there is higher or lower straigth cards in hand
                    // Aces will find straightdraws with 0... but i think its ok, gives them more value
                    else if (deadwood.Suit(cardsInHand[i]) == deadwood.Suit(cardsInHand[x]) && cardsInHand[i] == cardsInHand[x] + 1
                        || deadwood.Suit(cardsInHand[i]) == deadwood.Suit(cardsInHand[x]) && cardsInHand[i] == cardsInHand[x] - 1)
                    {
                        x = +20;
                        cardFound = true;
                    }
                    // Check gut draws also low cards will find gut draw with 0 but i think thats ok
                    else if (deadwood.Suit(cardsInHand[i]) == deadwood.Suit(cardsInHand[x]) && cardsInHand[i] == cardsInHand[x] + 2
                        || deadwood.Suit(cardsInHand[i]) == deadwood.Suit(cardsInHand[x]) && cardsInHand[i] == cardsInHand[x] - 2)
                    {
                        x = +20;
                        cardFound = true;
                    }
                }               
            }
            // Move card to returnCards if hits are not found
            if (cardFound == false)
            {
                // Putting one safe check just in case
                if (cardsInHand[i] != 0)
                {
                    returnCards[z] = cardsInHand[i];
                    z++;
                    singleCard = true;
                }   
            }
            cardFound = false;
        }
        return returnCards;
    }
    public void DropCard(int card, GameObject dropHere)
    {
        if (card != 0)
        {
            bool found = false;
            int spriteName = card - 1;  // Sprites start at 0
            string name = "Deck_" + spriteName;
            for (int i = 0; i < hand.transform.childCount; i++)
            {
                // Compare sprites name to card
                if (name == hand.transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite.name)
                {
                    // Set parent to tabletop.transform so that the card goes to right place
                    hand.transform.GetChild(i).SetParent(dropHere.transform);
                    played = true;
                    if (Grid.game.debugMessages) Grid.game.deadwood.printCard(card);
                    if (Grid.game.debugMessages) Debug.Log("Dropped to " + dropHere.name);
                    found = true;
                    i = 20;     
                }
            }
            if (!found) Debug.Log("*************DROPCARD VITURALLAAN*******************");
            {

            }
        }
        else
        {
            if (Grid.game.debugMessages) Debug.Log("Dropcard = 0, not found");
        }  
    }
    public int FindHighestFace(int[] cards)
    {
        Deadwood deadwood = Grid.game.deadwood;
        for (int i = 13; i >= 1; i--)   // All suits from highest to lowest
        {
            for (int x = 0; x < cards.Length; x++)    // all cards in array
            {
                if (deadwood.Face(cards[x]) == i && cards[x] != 0)
                {
                    if (deadwood.Face(cards[x]) > 2) // Dont drop too low cards
                    {
                        return cards[x];
                    }
                }
            }
        }
        return 0;
    }
    // Check bot deadwood by changing turn to bot and checking hes hand. 
    // Set turn back to 0 and then try to put all deadwood hands to hand 0
    public void MoveDeadwoodToOpponentHand()
    {        
        DropZone.turn = 1;
        Deadwood deadwood = Grid.game.deadwood;
        SortCards sort = Grid.game.sort;
        int playerDeadwood = deadwood.publicDeadwood;
        sort.SortCardsButton();
        int[] cards = new int[20];
        for (int i = 0; i < deadwood.cardsInHand.Length; i++)
        {
            cards[i] = deadwood.cardsInHand[i]; //Have to move it this way cause countdeadwood is changing cardsinhand later
        }
        DropZone.turn = 0;
        deadwood.CountDeadwood(); // set deadwood back to player 0   
        if (Grid.game.debugMessages) Debug.Log("-----------------------DEADWOOD CARDS----------------");
        if (Grid.game.debugMessages) deadwood.printCards(cards);
        if (Grid.game.debugMessages) Debug.Log("-----------------------DEADWOOD CARDS ENDS----------------");
        for (int i = 0; i < cards.Length; i++)
        {
            if (cards[i] != 0)
            {
                if (playerDeadwood != 0 && deadwood.MoveDeadwoodToEnderHand(cards[i]))
                {
                    DropCard(cards[i], playerHand);
                    cards[i] = 0;   // remove the card
                    i = -1;
                }
            }
        endgame.SetActive(true);
        }
        if (Grid.game.onlyBots == true)
        {
            Grid.game.sceneController.NextDeal();
        }
    }
    public void PublishDeadwood()
    {
        DropZone.turn = 1;
        Grid.game.deadwood.CountDeadwood();
        botAction.text = "Bot lost with "+ Grid.game.deadwood.publicDeadwood + " deadwood";
        DropZone.turn = 0;
        Grid.game.deadwood.CountDeadwood();
        Grid.game.sceneController.nextDeal.SetActive(true);
        Grid.game.sceneController.endGame.SetActive(false);
    }
}
