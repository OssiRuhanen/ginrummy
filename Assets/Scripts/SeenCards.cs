﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeenCards : MonoBehaviour
{
    public static int[] seenCards1 = new int[100];
    public static int[] seenCards2 = new int[100];
    public static int slot1 = 0; 
    public static int slot2 = 0;

    private void Start()
    {
        slot1 = 0;
        slot2 = 0;
    }
    public static void CardtoSeenCards(GameObject card, int array)
    {
        Sprite sprite = card.transform.GetChild(0).GetComponent<Image>().sprite;
        int i = 0;
        while (i < 52)
        {  
            // Try all sprite names to find out what card it is
            if (sprite.name == "Deck_" + i)   
            {
                if (array == 1)
                {
                    seenCards1[slot1] = i+1;
                    slot1++;
                }
                else
                {
                    seenCards2[slot2] = i+1;
                    slot2++;
                }
            }
            i++;
        }    
    }
    public void PrintSeenCards()
    {
        Grid.game.deadwood.printCards(seenCards1);
    }
}
