﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public Text winText;
    public Text lossText;
    public Text winrateText;
    // Start is called before the first frame update
    void Start()
    {
        int wins = PlayerPrefs.GetInt("WinAmount");
        int losses = PlayerPrefs.GetInt("LossAmount");
        int winrate = wins/(wins+losses);

        winText.text = "Wins: " + wins;
        lossText.text = "Losses: " + losses;
        winrateText.text = "Winrate: " + winrate;
    }


    public void StartGame()
    {
        ResetScores();
        SceneManager.LoadScene("Scene");
    }
    public void ResetScores()   // Reset PlayerPrefs when starting new game
    {
        PlayerPrefs.SetInt("PlayerScore", 0);
        PlayerPrefs.SetInt("EnemyScore", 0);
    }
}
