﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class Deadwood : MonoBehaviour
{
    public GameObject endGame;
    public GameObject nextDeal;
    public GameObject playerCards;
    public GameObject enemyCards;
    GameObject handCards;
    public Text deadwood;
    int notDeadwoodCount = 0;
    public int[] cardsInHand = new int[20];
    int[] cardSets = new int[20];    
    int[] cardSetsPlaceholder = new int[20];
    int[] countedCompletedSets = new int[20];
    public int[] countedCompletedSetsOld = new int[20];
    public int[] countedCompletedSetsEndgame = new int[20]; // did not use old ones for easy debugging
    public int deadwoodPairFirst;
    public int deadwoodStraightFirst;
    public int publicDeadwood;
    bool moveDeadwood = false;
    // Start is called before the first frame update
    public void Start()
    {
        notDeadwoodCount = 0;
        cardsInHand[10] = 999;  // Fixes problem when you drop a [10] card to tabletop
        clearArray(cardsInHand);    // testing
        updateHandcards();  // handcards = whos turn it is  
        CardsToArray(handCards); // Move cards from hand to handCards
        clearArray(cardSets);
        clearArray(cardSetsPlaceholder);
        clearArray(countedCompletedSets);
        notDeadwoodCount = 0;
        endGame.SetActive(false);
    }

    public void CountDeadwood() // Count deadwood from hand
    {
        Start(); // clear arrays
        deadwoodPairFirst = 0;
        deadwoodStraightFirst = 0;      
        // Count pairs first
        CountPairedDeadwood();
        CountStraightDeadwood();
        MoveArrayToArray(countedCompletedSets, countedCompletedSetsOld); // move counted to placeholder
        deadwoodPairFirst = DeadwoodCompare();
        // clear used arrays
        Start();
        // Count straights first
        CountStraightDeadwood();
        CountPairedDeadwood();
        deadwoodStraightFirst = DeadwoodCompare();

        if (deadwoodPairFirst < deadwoodStraightFirst)
        {
            if (DropZone.turn == 0 && !moveDeadwood) // !moveDeadwood so i wont override deadwood text when moving deadwood 
            {
                deadwood.text = "Deadwood: " + deadwoodPairFirst;
            }        
            publicDeadwood = deadwoodPairFirst;
            // Move them back for end turn enemy cards so i dont have to check lower deadwood
            MoveArrayToArray(countedCompletedSetsOld, countedCompletedSetsEndgame);    
        }
        else
        {
            if (DropZone.turn == 0 && !moveDeadwood) // !moveDeadwood so i wont override deadwood text when moving deadwood 
            {
                deadwood.text = "Deadwood: " + deadwoodStraightFirst;
            }    
            publicDeadwood = deadwoodStraightFirst;
            // Move them back for end turn enemy cards so i dont have to check lower deadwood
            MoveArrayToArray(countedCompletedSets, countedCompletedSetsEndgame);    
        }

        if (deadwoodPairFirst <= 10 || deadwoodStraightFirst <= 10)
        {
            endGame.SetActive(true);
        }
       
    }
    // Returns card suit int 0,1,2 or 3
    public int Suit(int card)
    {
        int suit = card / 13;
        if (card == 13)
        {
            suit = 0;
        }
        if (card == 26)
        {
            suit = 1;
        }

        if (card == 39)
        {
            suit = 2;
        }

        if (card == 52)
        {
            suit = 3;
        }
        return suit;
    }
    // Returns card face 1-13
    public int Face(int card)  
    {
        int face = card % 13;
        if (face == 0)
        {
            face = 13;
        }
        return face;
    }
    // Checks all sprites in hand and puts them into cardsInHand
    public void CardsToArray(GameObject hand)
    {
        int i = 0;
        while (i < hand.transform.childCount)
        {           
            Sprite card = hand.transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite;  // get card sprite
            int x = 0;
            while (x < 52)
            {
                if (card.name == "Deck_" + x)   // Try all sprite names to find out what card it is
                {
                    cardsInHand[i] = x+1;         // Set cardsInHand array to the hand found
                                                  // +1 cause sprites start from deck_0
                    break;
                }
                x++;
            }
            i++;
        }
    }

    // Finds higher face card
    void FindHigherStraight(int card)   
    {
        int y = 1;
        int x = 0;
        cardSetsPlaceholder[0] = card;   // put the first card in first spot
        for (int i = 0; i < handCards.transform.childCount; i++)
        {
            if (card == cardsInHand[i] - y && Suit(card) == Suit(cardsInHand[i]) && card > 0)   // Test if card is one higher and same suit
            {
                //Debug.Log(Face(cardsInHand[i]));
                cardSetsPlaceholder[x+1] = cardsInHand[i];
                x++;
                y++;
                i = -1; // so it starts from 0 again
                
            }
        }
        // if cards are found, placing them in cardSets
        if (CountArray(cardSetsPlaceholder) > 2)  // Check if array has 3 or more cards
        {
            int z = 0;
            for (int b = 0; b < cardSetsPlaceholder.Length; b++)
            {
                if (cardSetsPlaceholder[b] > 0)
                {
                    cardSets[z] = cardSetsPlaceholder[b];
                    z++;
                }
            }
            MoveToCountedDeadwood(cardSets);
        }
        clearArray(cardSetsPlaceholder);
    }


    void FindPair(int card)  // Search for same numbered cards
    {
        int x = 0;
        for (int i = 0; i < handCards.transform.childCount; i++)    // Check all cards
        {
            if (Face(card) == Face(cardsInHand[i]) && card > 0 && card != 999)    // If card has the same number as card in hand, first one always hits
            {
                cardSetsPlaceholder[x] = cardsInHand[i]; // Put the card in cardSetsPlaceholder
                x++;
            }
        }
        // if cards are found, placing them in cardSets
        if (CountArray(cardSetsPlaceholder) > 2)  // Check if array has 3 or more cards
        {
            int z = 0;
            for (int b = 0; b < cardSetsPlaceholder.Length; b++)
            {
                if (cardSetsPlaceholder[b] > 0)
                { 
                    cardSets[z] = cardSetsPlaceholder[b];       
                    z++;
                }
            }
            MoveToCountedDeadwood(cardSets);
        }
        clearArray(cardSetsPlaceholder);
    }

    public void CountStraightDeadwood()     // Search for cards with higher number and same suit
    {
        for (int i = 0; i < cardsInHand.Length; i++)
        {
            FindHigherStraight(cardsInHand[i]);           
        }
        DeleteSets(cardsInHand);    // deleting cards from cardsInHand so FindPair doesnt use these cards
    } 
    

    public void CountPairedDeadwood()   // Search for cards with same number
    {
        for (int i = 0; i < cardsInHand.Length; i++)
        {
            FindPair(cardsInHand[i]);
        }
        DeleteSets(cardsInHand); // deleting cards from cardsInHand so FindHigherStraight doesn't use these cards
    }

    // Count the amount of over 0 cards in array
    public int CountArray(int[] array) 
    {
        int count = 0;
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i]>0)
            {
                count++;
                //Debug.Log("CountArray= " + count);
            }
        }
        return count;
    }

    // Print cards
    public void printCards(int[] cards) 
    { 
        foreach (int item in cards)
        {
            if (item > 0)
            {
                printCard(item);
            } 
           
        }
    }
    public void printCard(int card)
    {
        if (Suit(card) == 0)
        {
            Debug.Log(Face(card) + " Spades");
        }
        else if (Suit(card) == 1)
        {
            Debug.Log(Face(card) + " Hearts");
        }
        else if (Suit(card) == 2)
        {
            Debug.Log(Face(card) + " Diamonds");
        }
        else if (Suit(card) == 3)
        {
            Debug.Log(Face(card) + " Clubs");
        }
        
    }

    // Moves 3 or more cards to completed sets
    public void MoveToCountedDeadwood(int[] cards)  // moving hits from cardSets to counteddeadwood
    {
        for (int i = 0; i < cards.Length; i++)
        {
            if (cards[i] > 0)
            {
                if (countedCompletedSets.Contains(cards[i]) == false) // Checking if array already contains card
                {
                    countedCompletedSets[notDeadwoodCount] = cards[i];
                    notDeadwoodCount++;
                }
            }
        }
    }
    
    public int DeadwoodCompare()   // returns deadwood in hand
    {
        updateHandcards();
        int deadwood = 0;
        for (int i = 0; i <= handCards.transform.childCount; i++)   // <= because when trying to drop card to hand we need more count...
        {
            if (cardsInHand[i] > 0 && cardsInHand[i] != 999)  // count all leftover cards, take the placeholder [10] out too
            {
                if (Face(cardsInHand[i]) < 10 && Face(cardsInHand[i]) != 0) // King is 0... 
                {
                    deadwood = deadwood + Face(cardsInHand[i]);     
                }
                else
                {
                    deadwood = deadwood + 10 ;   // set all higher cards to 10
                }
            }
        }
        return deadwood;
    }
    public void DeleteSets(int[] cards)   // Delete cards that are completed sets from array
    {
        for (int i = 0; i < cardsInHand.Length; i++)
        {         
            if (cardsInHand.Contains(countedCompletedSets[i]) == true)
            {
                for (int x = 0; x < cardsInHand.Length; x++)   // find the card from hand
                {
                    if (cardsInHand[x] == countedCompletedSets[i])
                    {
                        cardsInHand[x] = 0;     // deletes hits from hand
                        x = 100;
                    }
                }
            }
        }
    }
    void clearArray(int[] cards)
    {
        for (int k = 0; k < cards.Length; k++) // clear placeholder
        {
            cards[k] = 0;
        }
    }
    void updateHandcards()
    {
        if (DropZone.turn == 0) // check turn to count right cards
        {
            handCards = playerCards;
        }
        else
        {
            handCards = enemyCards;
        }
    }
    void MoveArrayToArray(int[]movefrom, int[] moveto)
    {
        int i = 0;
        foreach (var item in movefrom)
        {
            moveto[i] = movefrom[i];
            i++;
        }
    }
    // Tests if enemy cards hit the completedsets
    // So it checks all cards that form a set from the hand, so deadwood = 0
    // and then it puts the card in those cards and tests again, if deadwood = 0
    // it puts the card in
    public bool MoveDeadwoodToEnderHand(int card) 
    {
        // Check if it was bot who ended so no need to update deadwood text
        if (SceneController.botEnded == true)
        {
            moveDeadwood = true;
        }
        
        deadwoodPairFirst = 0;
        deadwoodStraightFirst = 0;
        clearArray(cardsInHand);    // clear for only completed sets
        int x = 0;
        for (int i = 0; i < CountArray(countedCompletedSetsEndgame); i++) // All the cards in completedsets that are over 0
        {
            if (countedCompletedSetsEndgame[i] != 0)
            {
                cardsInHand[i] = countedCompletedSetsEndgame[i];  // Move all cards to cardsInHand
                //printCard(cardsInHand[i]);
                x++;    // So the Card goes to last spot
            }           
        }
        cardsInHand[x + 1] = card;  // cardsInHand is now set for testing
        // Count pairs first
        CountPairedDeadwood();
        CountStraightDeadwood();
        MoveArrayToArray(countedCompletedSets, countedCompletedSetsOld); // move counted to placeholder
        deadwoodPairFirst = DeadwoodCompare();
        clearArray(cardSets);
        clearArray(cardSetsPlaceholder);
        clearArray(countedCompletedSets);
        notDeadwoodCount = 0;
        x = 0;
        // Next test again but with straights first
        // All the cards in completedsets that are over 0
        for (int i = 0; i < CountArray(countedCompletedSetsEndgame); i++) 
        {
            cardsInHand[i] = countedCompletedSetsEndgame[i];  // Move all cards to cardsInHand
            x++;    // So the Card goes to last spot
        }
        cardsInHand[x + 1] = card;  // cardsInHand is now set for testing
        CountStraightDeadwood();
        CountPairedDeadwood();
        deadwoodStraightFirst = DeadwoodCompare();
        if (deadwoodPairFirst < deadwoodStraightFirst)
        {
            // Move them back for end turn enemy cards so i dont have to check lower deadwood
            MoveArrayToArray(countedCompletedSetsOld, countedCompletedSetsEndgame);
        }
        else
        {
            // Move them back for end turn enemy cards so i dont have to check lower deadwood
            MoveArrayToArray(countedCompletedSets, countedCompletedSetsEndgame);
        }
        if (deadwoodPairFirst == 0 || deadwoodStraightFirst == 0)
        {
            if (Grid.game.debugMessages) printCard(card);
            if (Grid.game.debugMessages) Debug.Log("Deadwood hits");
            return true;
        }
        else
        {
            return false;
        }
    }
    // Updating deadwood text when dropping cards to opponents hand without counting deadwood
    public void UpdateDeadwoodWhenCardDropped(GameObject card)
    {
        string findnumber = deadwood.text;
        string numbersOnly = Regex.Replace(findnumber, "[^0-9]", "");
        int.TryParse(numbersOnly, out int deadwoodtext);
        int minusdeadwood = 0;
        Sprite cardSprite = card.transform.GetChild(0).GetComponent<Image>().sprite;  // get card sprite
        int cardInt = 0;
        int x = 0;
        while (x < 52)
        {
            if (cardSprite.name == "Deck_" + x)   // Try all sprite names to find out what card it is
            {
                cardInt = x + 1;
                x =+ 52;
            }
            x++;
        }      
        if (Face(cardInt) < 10 && Face(cardInt) != 0) // King is 0... 
        {
            minusdeadwood = Face(cardInt);
        }
        else
        {
            minusdeadwood =+ 10;   // set all higher cards to 10
        }
        deadwoodtext = deadwoodtext - minusdeadwood;
        deadwood.text = "Deadwood: " + deadwoodtext;
    }
}

