﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropZone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler {
    static public bool drawCard = false;
    static public bool playCard = false;
    static public bool drawSecond = false;
    static public int turn = 0;
    public int enemyCard;

    void Start()
    {
        drawCard = false;
        playCard = false;
        drawSecond = false;
    }

    public void OnPointerEnter(PointerEventData eventData) {
		if(eventData.pointerDrag == null)
			return;
		Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
        // When you start to drag from drawdeck set it inactive
        // so you cant just look cards in drawdeck and drop them back
        if (gameObject.name == "Drawdeck" && d.parentToReturnTo.name != "Hand")
        {
            Grid.game.drawDeck.SetActive(false);
        }
        else if (gameObject.name == "Tabletop" && d.parentToReturnTo.name != "Hand")
        {
            Grid.game.tabletop.SetActive(false);
        }

		if(d != null) {
			d.placeholderParent = this.transform;
		}      
	}
	
	public void OnPointerExit(PointerEventData eventData) {
        // Destroy cardback if theres no cards in drawdeck
        if (gameObject.name == "Drawdeck" && gameObject.transform.childCount < 2)   
        {
            Destroy(GameObject.Find("CardBack"));
        }
        if (eventData.pointerDrag == null)
			return;

		Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
		if(d != null && d.placeholderParent==this.transform) {
			d.placeholderParent = d.parentToReturnTo;
		}
	}
	
	public void OnDrop(PointerEventData eventData) {     // On card drop    
        // First finding needed components      
        GameObject sceneController = GameObject.Find("SceneController");
        Deadwood deadwood = sceneController.GetComponent<Deadwood>();
        Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
        if (gameObject.tag == "Hand" && Draggable.notFromHand == true)  // when you draw card to hand
        {
            GameObject CardBack = GameObject.Find("CardBack");  // set CardBacks raycast true so you cant draw
            CardBack.transform.GetComponent<Image>().raycastTarget = true;
            drawCard = true;
            Grid.game.drawDeck.SetActive(false);           
        }
        if (gameObject.name == "Drawdeck")  // If you try to drop card to drawdeck
        {
            if (Grid.game.debugMessages) Debug.Log(d.parentToReturnTo.ToString());
        }
        if (gameObject.name == "EnemyHand" && turn == 1)    // If you try to drop to enemy hand
        {
            if (SceneController.roundEnded) // roundended painettu
            {
                GameObject card = eventData.pointerDrag;
                Sprite cardSprite = card.transform.GetChild(0).GetComponent<Image>().sprite;  // get card sprite
                enemyCard = 0;
                int x = 0;
                while (x < 52)
                {
                    if (cardSprite.name == "Deck_" + x)   // Try all sprite names to find out what card it is
                    {
                        enemyCard = x + 1;         // Set enemyCard
                        x = +52;
                        if (Grid.game.debugMessages) Debug.Log(enemyCard);      // +1 cause sprites start from deck_0
                    }
                    x++;
                }
                // Test if the players card fits in enemy hand
                if (deadwood.MoveDeadwoodToEnderHand(enemyCard) && deadwood.publicDeadwood != 0)
                {
                    // Update deadwood text
                    deadwood.UpdateDeadwoodWhenCardDropped(eventData.pointerDrag);
                    d.parentToReturnTo = this.transform;
                    Grid.game.tabletop.SetActive(false);  // set tabletop false so you cant draw again
                }
            }
        }
        else if (gameObject.name == "Hand" && turn == 0)   // If enemy tries to drop to your hand
        {
            if (SceneController.roundEnded) // roundended painettu
            {
                GameObject card = eventData.pointerDrag;
                Sprite cardSprite = card.transform.GetChild(0).GetComponent<Image>().sprite;  // get card sprite
                enemyCard = 0;
                int x = 0;
                while (x < 52)
                {
                    if (cardSprite.name == "Deck_" + x)   // Try all sprite names to find out what card it is
                    {
                        enemyCard = x + 1;         // Set enemyCard
                        if (Grid.game.debugMessages) Debug.Log(enemyCard);      // +1 cause sprites start from deck_0
                    }
                    x++;
                }
                // Test if the enemyCard fits in completedsets
                if (deadwood.MoveDeadwoodToEnderHand(enemyCard) && deadwood.publicDeadwood != 0)
                {
                    d.parentToReturnTo = this.transform;
                }
            }
        }
        else if (drawCard == false)  // If you try to drop a card to tabletop before drawing
        {
            if (Grid.game.debugMessages) Debug.Log("Haven't drawn yet");
        }
        else if (playCard == true && gameObject.tag == "Draw")          // If you try to drop another card to tabletop
        {
            if (Grid.game.debugMessages) Debug.Log("Trying to drop another card on tabletop");
        }
        else if (playCard == true && gameObject.tag != "Draw")        // If you try to draw another card
        {
            if (Grid.game.debugMessages) Debug.Log("Trying to draw another card from tabletop");
        }

        else if (d != null) {
			d.parentToReturnTo = this.transform;
            drawSecond = true;
		}
        if (gameObject.name == "Tabletop" && drawCard == true)  // When you play a card
        {
            playCard = true;
            deadwood.CountDeadwood();
            // Show next turn button
            sceneController.GetComponent<SceneController>().nextTurn.SetActive(true); 
        }
    }
}
