﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class BotVsBot : MonoBehaviour
{
    bool playGame = true;
    
    public void StartBotGameButton()
    {   
        PlayBotGame(Grid.game.bot1, Grid.game.bot2);
    }
    public void PlayBotGame(AIController bot1, AIController2 bot2)
    {
        playGame = true;
        Grid.game.onlyBots = true;
        while (playGame && !Grid.game.waitLoad)
        {
            if (DropZone.turn == 0)
            {
                bot2.PlayTurn();    // Only need bot2 play cause bot1 always plays anyways
            }

            if (Grid.game.botRounds == 0)
            {
                playGame = false;
            }
            if (Grid.game.singleTurnsBot) playGame = false;
        }
    }
}
